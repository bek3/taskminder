#!/usr/bin/env bash

# Systemwide Taskminder install script
# Created by Brendan Kristiansen on 10 April 2019
# Updated on 12 July 2019

tm_version="3.2.1"

function create_dir() {
	dir=$1

	if [ ! -d $dir ]
	then
		mkdir -v $dir
	else
		echo "$dir already exists"
	fi
}

echo "Installing Taskminder"

cp -v src/taskminder.sh /usr/bin/taskminder
cp -v src/tm-sync.sh /usr/bin/tm-sync
sudo chmod 777 /usr/bin/taskminder
sudo chmod 777 /usr/bin/tm-sync

create_dir ~/.taskminder/tasks/
create_dir ~/.taskminder/tasks/cats/

if [ -e /etc/taskminder-config ]
then
	echo "Removing old configuration"
	rm -v /etc/taskminder-config
fi

echo "Generating configuration"
echo "# Taskminder configuration" >> /etc/taskminder-config
echo "tmpath=~/.taskminder" >> /etc/taskminder-config
echo "taskpath=~/.taskminder/tasks" >> /etc/taskminder-config
echo "tm_version=$tm_version" >> /etc/taskminder-config
echo"" >> /etc/taskminder-config
echo "if [ ! -d ~/.taskminder/tasks ]" >> /etc/taskminder-config
echo "then" >> /etc/taskminder-config
echo "	mkdir -v ~/.taskminder/" >> /etc/taskminder-config
echo "	mkdir -v ~/.taskminder/tasks" >> /etc/taskminder-config
echo "fi" >> /etc/taskminder-config

echo "Installation complete"
