#!/usr/bin/env bash

# Taskminder install script
# Created by Brendan Kristiansen on 01 November 2018
# Updated on 12 July 2019

tm_version="3.2.1"

function create_dir() {
	dir=$1

	if [ ! -d $dir ]
	then
		mkdir -v $dir
	else
		echo "$dir already exists"
	fi
}

if [ ! -d $HOME/bin/ ]
then
	mkdir -v $HOME/bin
fi

echo "Installing Taskminder"

cp -v src/taskminder.sh ~/bin/taskminder
cp -v src/tm-sync.sh ~/bin/tm-sync
chmod 755 ~/bin/taskminder
chmod 755 ~/bin/tm-sync

create_dir ~/.taskminder/
create_dir ~/.taskminder/tasks/
create_dir ~/.taskminder/tasks/cats/

if [ -e ~/.taskminder/config ]
then
	echo "Removing old configuration"
	rm ~/.taskminder/config
fi

echo "Generating configuration"
echo "# Taskminder configuration" >> ~/.taskminder/config
echo "tmpath=~/.taskminder" >> ~/.taskminder/config
echo "taskpath=~/.taskminder/tasks" >> ~/.taskminder/config
echo "tm_version=$tm_version" >> ~/.taskminder/config

echo "Installation complete"
