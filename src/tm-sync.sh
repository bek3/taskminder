#!/usr/bin/env bash

# Taskminder Sync
# Created by Brendan Kristiansen on 2 November 2018
# Updated: 10 April 2019

if [ -e /etc/taskminder-config ]
then
	source /etc/taskminder-config
elif [ -e ~/.taskminder/config ]
then
	source ~/.taskminder/config
else
	echo "No valid configuration found."
	exit -1
fi

source ~/.taskminder/config

if [ -d $taskpath/.git/ ]
then
	cd $taskpath/

	echo "Recieving changes..."
	git pull origin master
	echo "Updating remote..."
	git add .
	git commit -m "Sync from $(hostname) at $(date +%s)"
	git push origin master

	cd -

elif [ -d $taskpath/.hg/ ]
then
	cd $taskpath/
	echo "Recieving changes..."
	hg pull; hg update
	echo "Updating remote..."
	hg addremove
	hg commit -m "Sync from $(hostname) at $(date +%s)"
	hg push
	cd -
else
	echo "Initializing taskpath for synchronization."
	read -p "Enter 1 for Git or 2 for Mercurial: " vcs
	read -p "Entier URL for repository: " url

	if (( vcs == 1 ))
	then
		cd $taskpath
		git init
		git remote add origin $url
		cd -
	elif (( vcs == 2 ))
	then
		cd $taskpath
		hg init
		echo "[paths]" >> .hg/hgrc
		echo "default = $url" >> .hg/hgrc
		cd -
	else
		echo "Invalid option."
	fi

	tm-sync
fi

if [ -e $tmpath/sync-flag ]
then
	echo "Resetting sync flag..."
	rm $tmpath/sync-flag
fi

echo "Sync complete"
