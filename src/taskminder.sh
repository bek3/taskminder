#!/usr/bin/env bash

# Taskminder
# Created by Brendan Kristiansen on 1 November 2018
# Updated: 12 July 2019

if [ -e /etc/taskminder-config ]
then
	source /etc/taskminder-config
elif [ -e ~/.taskminder/config ]
then
	source ~/.taskminder/config
else
	echo "No valid configuration found"
	exit -1
fi

exec_version="3.2.1"

function new_task {
	read -p "Enter task name: " name_in
	read -p "Enter task category: " ctgy_in
	read -p "Describe task: " desc_in
	
	for ctgy in $taskpath/cats/*;
	do
		if [ "$ctgy" = "$taskpath/cats/$ctgy_in" ]
		then
			id=$(date +%s)
			task_dir=$taskpath/$id

			mkdir $task_dir; cd $task_dir

			echo $id > $task_dir/id
			echo $name_in > $task_dir/name
			echo $desc_in > $task_dir/desc
			echo $ctgy_in > $task_dir/ctgy

			cd - > /dev/null

			exit 0
		fi
	done

	echo "Invalid category."
}

if [ $# -eq 0 ]
then
	echo "Taskminder Version $exec_version"
	echo ""
	echo "Usage:"
	echo "	no arguments		Display this message"
	echo "	summary			Display number of tasks"
	echo "	ls			List all stored tasks"
	echo "	new			Create a new task"
	echo "	sync			Synchronize tasks"
	echo "	flag-sync		Set reminder to synchronize"
	echo "	comp [id]		Mark task as complete"
	echo ""
	echo "Category Management:"
	echo "	new-cat			Create new category"
	echo "	ls-cat			List current task categories"
	echo "	del-cat			Delete category"
	echo ""
elif [ $# -eq 1 ]
then
	if [ $1 = "summary" ]
	then
		cd $taskpath
		tasks=$(find . -mindepth 1 -maxdepth 1 ! -name '.*' -type d | wc -l)
		cd - > /dev/null
		echo "Taskminder: $(expr $tasks - 1) incomplete. Run 'taskminder ls' for details."

		if [ -e $tmpath/sync-flag ]
		then
			echo "Taskminder: Synchronization required."
		fi

	elif [ "$1" = "ls" ]
	then
		echo "Taskminder tasks:"
		printf "| %-15s | %-12s | %-25s | %-55s |\n" "ID" "Category" "Name" "Description"
		echo "+-----------------+--------------+---------------------------+---------------------------------------------------------+"
		for task in $taskpath/*;
		do
			if [ $task = "$taskpath/cats" ]
			then
				continue
			else
				if [ -e $task/id ]
				then
					id=$(cat $task/id)
					name=$(cat $task/name)
					desc=$(cat $task/desc)
					ctgy=""
					
					if [ -e $task/ctgy ]
					then
						ctgy=$(cat $task/ctgy)
					fi

					printf "| %-15s | %-12s | %-25s | %-55s |\n" "$id" "$ctgy" "$name" "$desc"
				else
					echo "No tasks to show"
				fi
			fi
		done
	elif [ "$1" = "new" ]
	then
		new_task
	elif [ "$1" = "sync" ]
	then
		tm-sync
	elif [ "$1" = "flag-sync" ]
	then
		if [ ! -e $tmpath/sync-flag ]
		then
			touch $tmpath/sync-flag
			echo "Sync reminder set!"
		else
			echo "Sync reminder already set!"
		fi
	elif [ "$1" = "list" ]		# Redundancy for `taskminder ls`
	then
		taskminder ls
	elif [ "$1" = "comp" ]
	then
		echo "Task ID required"
	elif [ "$1" = "complete" ]
	then
		taskminder comp
	elif [ "$1" = "new-cat" ]
	then
		echo "Create new Category"
		read -p "Enter name for new category: " name

		cd $taskpath/cats
		count=$(find . ! -name '.*' -mindepth 1 -maxdepth 1 -type d | wc -l)
		cd - > /dev/null

		if [ "$count" != "0" ]
		then
			for category in $taskpath/cats;
			do
				exist_name=$category

				if [ "$name" = "$exist_name" ]
				then
					echo "Category already exists."
					exit 0
				fi
			done
		fi
		touch $taskpath/cats/$name
	elif [ "$1" = "ls-cat" ]
	then
		echo "Current categories:"
		ls $tmpath/tasks/cats
	elif [ "$1" = "del-cat" ]
	then
		read -p "Enter category to delete: " del_cat
		if [ -e $taskpath/cats/$del_cat ]
		then
			rm -v $taskpath/cats/$del_cat

		else
			echo "Invalid category."
		fi
	else
		echo "Invalid Argument"
	fi
elif [ $# -eq 2 ]
then
	if [ "$1" = "comp" ]
	then
		comp_task=$2
		if [ -d $taskpath/$comp_task ]
		then
			echo "Completed task '$(cat $taskpath/$comp_task/name)'"
			rm -r $taskpath/$comp_task
		else
			echo "Invalid task ID"
		fi
	elif [ "$1" = "complete" ]	# Redundancy for `taskminder comp`
	then
		taskminder comp $2
	else
		echo "Invalid Argument"
	fi
else
	echo "Invalid Arguments"
fi
