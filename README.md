# Taskminder

A simple to-do list for your shell.

## Usage

Taskminder allows you to manage your todo list in your shell. For help, simply run
`taskminder` and accepted usage will be displayed.

### Summarize your to-do list

Run `taskminder summary` to be reminded how many current items are in your to-do list.

### List all tasks

To recieve a list of all tasks currently stored in Taskminder, run `taskminder ls` or
`taskminder list`. This will list each task sorted by ID with its name and description.

### Create a new task

To create a new task, run `taskminder new`. Taskminder will prompt for a name and a description.
After the user has entered this information, taskminder will use the current epoch time as
an ID number for the task. This information will be stored in the task directory and can be
verified with `taskminder ls`.

### Mark task as completed

To mark a task as complete, run `taskminder comp` or `taskminder complete` followed by the
ID number of the task completed. This can be found by running `taskminder ls`.

### Be reminded of taskminder at startup

To be reminded of Taskmninder every time you start your shell, place `taskminder summary` in
the appropriate rc file for your shell.

## Syncrhonization via Git

### Git repository requirements

It is best to dedicate a private git repository to hosting your to-do list.

### Usage

Run `taskminder sync` or `tm-sync` to pull other tasks and push any new tasks. If you have not
configured synchronization, this command will automatically start configuration. While setting
up sync, the script will prompt for the url of your repository to hold your tasks. Enter the url
as you would if you were using `git clone`. The script will automatically pull any other tasks in
the repository and push your new tasks. Commit messages will contain your hostname and the epoch
time of the sync.

#### Synchronization reminders

For a reminder in your shell to sync your tasks after offline changes, run `taskminder flag-sync`.
This will set a reminder in your shell on every login until you are able to synchronize your
task list.

## Changelog

| Version	| Date		| Changes						|
|:--------------|---------------|:------------------------------------------------------|
| 3.2.1		| 12 Jul. 2019	| Fix order of arguments to find command					|
| 3.2.0		| 10 Apr. 2019	| Enable systemwide taskminder install			|
| 3.1.2		| 01 Apr. 2019	| Clean category search in tm-new			|
| 3.1.1		| 25 Mar. 2019	| Make line lengths more generous in ls option		|
| 3.1.0		| 16 Mar. 2019	| Add Mercurial support for task synchronization	|
| 3.0.1		| 15 Mar. 2019	| Reformat `ls`, Fix counting error from categories.	|
| 3.0.0		| 14 Mar. 2019	| Add support for task categories.			|
| 2.2.0		| 30 Dec. 2018	| Build sync reminder support.				|
| 2.1.1		| 30 Dec. 2018	| Redundancies for ls and comp. New error messages.	|
| 2.1.0		| 19 Dec. 2018	| Fix error message when listing with no tasks		|
| 2.0.3		| 5 Nov. 2018	| More generous spacing in list view			|
| 2.0.2		| 3 Nov. 2018	| Fix shell specification in scripts			|
| 2.0.1		| 2 Nov. 2018	| Fix off by one error in summary			|
| 2.0		| 2 Nov. 2018	| Create support for synchronizaton			|
| 1.0		| 1 Nov. 2018	| Initial Release					|

## Future Improvements

* Support for synchronization via a Mercurial repository
